/*
 * Copyright (c) 2018 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef QUOTIENT_COMPLEX_H_
#define QUOTIENT_COMPLEX_H_

#include "mantis/complex.hpp"

namespace quotient {

using mantis::Complex;

}  // namespace quotient

#endif  // ifndef QUOTIENT_COMPLEX_H_
