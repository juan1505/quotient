![](./images/gabriel-40pct.png)

**quotient** is a C++14 header-only implementation of the (Approximate)
Minimum Degree reordering algorithm. Loosely speaking it is applicable for
sparse Symmetric Quasi-SemiDefinite (SQSD) Cholesky factorizations.

[![Join the chat at https://gitter.im/hodge_star/community](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/hodge_star/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Documentation](https://readthedocs.org/projects/quotient/badge/?version=latest)](https://hodgestar.com/quotient/docs/master/)

### Dependencies
There are no dependencies for installation of the headers.

But running the example drivers requires previous installation of the
header-only command-line processing C++14 library
[specify](https://gitlab.com/hodge_star/specify).

The build system for the examples uses [meson](http://mesonbuild.com) and
the unit tests use [Catch2](https://github.com/catchorg/Catch2).

### Quickstart
Please see the
[quickstart documentation](https://hodgestar.com/quotient/docs/master/quickstart.html)
for a brief introduction to the library.

### License
`quotient` is distributed under the
[Mozilla Public License, v. 2.0](https://www.mozilla.org/media/MPL/2.0/index.815ca599c9df.txt).
